var express = require('express');
var cheerio = require('cheerio');
var router = express.Router();
var request = require('request');

/* GET NewsItem. */
router.get('/api/news/:id', function (req, res) {
  var URL = 'https://www.057.ua/news/' + req.params.id;
  request(URL, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      var $ = cheerio.load(body);
      var news = $('.conteiner .static').text();
      res.render('newsItem', {
        newsItem: news
      });
    }
  });
});
/* GET News. */
router.get('/api/news', function (req, res) {
  var URL = 'https://www.057.ua/rss';
  request(URL, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      var $ = cheerio.load(body, {
        xmlMode: true
      });
      var news = [];
      $('item').slice(0, 5).each(function () {
        var imgs = [];
        var enclosure = $('enclosure', this);
        if (enclosure.length > 0) {
          enclosure.each(function () {
            imgs.push($(this).attr('url'));
          });
        }

        news.push({
          title: $('title', this).text(),
          url: $('link', this).text(),
          description: $('description', this).text(),
          imgsArr: imgs
        });
      });

      res.render('news', {
        newsArr: news
      });
    }
  });
});

/* GET home page. */
router.get('/', function (req, res) {
  res.render('index');
});

module.exports = router;
