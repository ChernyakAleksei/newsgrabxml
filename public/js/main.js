$(function () {
  $('.link-news').on('click', function (e) {
    e.preventDefault();
    var url = $(this).attr('href');
    var indexId = url.lastIndexOf('/') + 1;
    var idNews = Number(url.substring(indexId));
    window.location = '/api/news/' + idNews;
  });
});
